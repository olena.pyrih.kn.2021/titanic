import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract('(Mr\.|Mrs\.|Miss\.)')
    mean_age_by_title = df.groupby('Title')['Age'].mean()
    df['Age'].fillna(df['Title'].map(mean_age_by_title), inplace=True)
    age_filling_values = [
        (title, df[df['Title'] == title]['Age'].isna().sum(), mean_age)
        for title, mean_age in mean_age_by_title.items()
    ]
    return age_filling_values
